package main

import (
	b64 "encoding/base64"

	"github.com/pulumi/pulumi-kubernetes/sdk/v2/go/kubernetes"
	appsv1 "github.com/pulumi/pulumi-kubernetes/sdk/v2/go/kubernetes/apps/v1"
	corev1 "github.com/pulumi/pulumi-kubernetes/sdk/v2/go/kubernetes/core/v1"
	metav1 "github.com/pulumi/pulumi-kubernetes/sdk/v2/go/kubernetes/meta/v1"
	"github.com/pulumi/pulumi-linode/sdk/v2/go/linode"
	"github.com/pulumi/pulumi/sdk/v2/go/pulumi"
	"github.com/pulumi/pulumi/sdk/v2/go/pulumi/config"
)

func main() {
	pulumi.Run(func(ctx *pulumi.Context) error {
		conf := config.New(ctx, "")
		commonName := conf.Require("common_name")

		cluster, err := linode.NewLkeCluster(ctx, commonName, &linode.LkeClusterArgs{
			K8sVersion: pulumi.String("1.20"),
			Label:      pulumi.String(commonName),
			Pools: linode.LkeClusterPoolArray{
				&linode.LkeClusterPoolArgs{
					Count: pulumi.Int(3),
					Type:  pulumi.String("g6-standard-1"),
				},
			},
			Region: pulumi.String("eu-west"),
			Tags: pulumi.StringArray{
				pulumi.String("staging"),
				pulumi.String("lke-yetiops"),
			},
		})

		if err != nil {
			return err
		}

		lke_config := cluster.Kubeconfig.ApplyString(func(kconf string) string {
			output, _ := b64.StdEncoding.DecodeString(kconf)
			return string(output)
		})

		kubeProvider, err := kubernetes.NewProvider(ctx, commonName, &kubernetes.ProviderArgs{
			Kubeconfig: lke_config,
		}, pulumi.DependsOn([]pulumi.Resource{cluster}))

		if err != nil {
			return err
		}

		appLabels := pulumi.StringMap{
			"app": pulumi.String("nginx"),
		}

		namespace, err := corev1.NewNamespace(ctx, "app-ns", &corev1.NamespaceArgs{
			Metadata: &metav1.ObjectMetaArgs{
				Name: pulumi.String(commonName),
			},
		}, pulumi.Provider(kubeProvider))

		if err != nil {
			return err
		}

		deployment, err := appsv1.NewDeployment(ctx, "app-dep", &appsv1.DeploymentArgs{
			Metadata: &metav1.ObjectMetaArgs{
				Namespace: namespace.Metadata.Elem().Name(),
			},
			Spec: appsv1.DeploymentSpecArgs{
				Selector: &metav1.LabelSelectorArgs{
					MatchLabels: appLabels,
				},
				Replicas: pulumi.Int(1),
				Template: &corev1.PodTemplateSpecArgs{
					Metadata: &metav1.ObjectMetaArgs{
						Labels: appLabels,
					},
					Spec: &corev1.PodSpecArgs{
						Containers: corev1.ContainerArray{
							corev1.ContainerArgs{
								Name:  pulumi.String("nginx"),
								Image: pulumi.String("nginx"),
							}},
					},
				},
			},
		}, pulumi.Provider(kubeProvider))

		if err != nil {
			return err
		}

		ctx.Export("name", deployment.Metadata.Elem().Name())
		ctx.Export("kubeConfig", pulumi.Unsecret(lke_config))

		return nil
	})
}
