module basic-metal

go 1.14

require (
	github.com/pulumi/pulumi-cloudinit/sdk v0.2.1
	github.com/pulumi/pulumi-equinix-metal/sdk v1.1.1
	github.com/pulumi/pulumi/sdk/v2 v2.20.0
)
