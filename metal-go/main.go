package main

import (
	"fmt"
	"io/ioutil"
	"os/user"

	"github.com/pulumi/pulumi-cloudinit/sdk/go/cloudinit"
	metal "github.com/pulumi/pulumi-equinix-metal/sdk/go/equinix"
	"github.com/pulumi/pulumi/sdk/v2/go/pulumi"
	"github.com/pulumi/pulumi/sdk/v2/go/pulumi/config"
)

func main() {
	pulumi.Run(func(ctx *pulumi.Context) error {
		conf := config.New(ctx, "")
		commonName := conf.Require("common_name")
		cloudInitPath := conf.Require("cloud_init_path")

		cloudInitScript, err := ioutil.ReadFile(cloudInitPath)

		if err != nil {
			return err
		}

		cloudInitContents := string(cloudInitScript)

		b64encEnable := false
		gzipEnable := false
		contentType := "text/cloud-config"
		fileName := "init.cfg"
		cloudconfig, err := cloudinit.LookupConfig(ctx, &cloudinit.LookupConfigArgs{
			Base64Encode: &b64encEnable,
			Gzip:         &gzipEnable,
			Parts: []cloudinit.GetConfigPart{
				cloudinit.GetConfigPart{
					Content:     cloudInitContents,
					ContentType: &contentType,
					Filename:    &fileName,
				},
			},
		}, nil)

		if err != nil {
			return err
		}

		user, err := user.Current()

		if err != nil {
			return err
		}

		sshkey_path := fmt.Sprintf("%v/.ssh/id_rsa.pub", user.HomeDir)

		sshkey_file, err := ioutil.ReadFile(sshkey_path)
		if err != nil {
			return err
		}

		sshkey_contents := string(sshkey_file)

		sshkey, err := metal.NewSshKey(ctx, commonName, &metal.SshKeyArgs{
			Name:      pulumi.String(commonName),
			PublicKey: pulumi.String(sshkey_contents),
		})

		project, err := metal.NewProject(ctx, "my-project", &metal.ProjectArgs{
			Name: pulumi.String("yetiops-blog"),
		})
		if err != nil {
			return err
		}

		project_id := pulumi.Sprintf("%v", project.ID())

		_, err = metal.NewSpotMarketRequest(ctx, commonName, &metal.SpotMarketRequestArgs{
			//			ProjectId: project_id.ApplyString(func(id string) string {
			//				return id
			//			}),
			ProjectId:   project_id,
			MaxBidPrice: pulumi.Float64(0.20),
			Facilities: pulumi.StringArray{
				pulumi.String("fr2"),
			},
			DevicesMin: pulumi.Int(1),
			DevicesMax: pulumi.Int(1),
			InstanceParameters: metal.SpotMarketRequestInstanceParametersArgs{
				Hostname:        pulumi.String(commonName),
				BillingCycle:    pulumi.String("hourly"),
				OperatingSystem: pulumi.String("ubuntu_20_04"),
				Plan:            pulumi.String("c3.small.x86"),
				UserSshKeys: pulumi.StringArray{
					pulumi.StringOutput(sshkey.OwnerId),
				},
				Userdata: pulumi.String(cloudconfig.Rendered),
			},
		})

		ctx.Export("projectName", project.Name)
		return nil
	})
}
