module do-go

go 1.14

require (
	github.com/pulumi/pulumi-cloudinit/sdk v0.2.1
	github.com/pulumi/pulumi-digitalocean/sdk/v3 v3.7.0
	github.com/pulumi/pulumi/sdk/v2 v2.20.0
)
