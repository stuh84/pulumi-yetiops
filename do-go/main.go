package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"os/user"
	"strconv"
	"strings"

	"github.com/pulumi/pulumi-cloudinit/sdk/go/cloudinit"
	"github.com/pulumi/pulumi-digitalocean/sdk/v3/go/digitalocean"
	"github.com/pulumi/pulumi/sdk/v2/go/pulumi"
	"github.com/pulumi/pulumi/sdk/v2/go/pulumi/config"
)

func getMyIp() (string, error) {
	resp, err := http.Get("https://ifconfig.co")
	if err != nil {
		return "", err
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}

	MyIp := strings.TrimSuffix(string(body), "\n")

	return MyIp, nil
}

func main() {
	pulumi.Run(func(ctx *pulumi.Context) error {
		conf := config.New(ctx, "")
		commonName := conf.Require("common_name")
		cloudInitPath := conf.Require("cloud_init_path")

		cloudInitScript, err := ioutil.ReadFile(cloudInitPath)

		if err != nil {
			return err
		}

		cloudInitContents := string(cloudInitScript)

		b64encEnable := false
		gzipEnable := false
		contentType := "text/cloud-config"
		fileName := "init.cfg"
		cloudconfig, err := cloudinit.LookupConfig(ctx, &cloudinit.LookupConfigArgs{
			Base64Encode: &b64encEnable,
			Gzip:         &gzipEnable,
			Parts: []cloudinit.GetConfigPart{
				cloudinit.GetConfigPart{
					Content:     cloudInitContents,
					ContentType: &contentType,
					Filename:    &fileName,
				},
			},
		}, nil)

		if err != nil {
			return err
		}

		user, err := user.Current()

		if err != nil {
			return err
		}

		sshkey_path := fmt.Sprintf("%v/.ssh/id_rsa.pub", user.HomeDir)

		sshkey_file, err := ioutil.ReadFile(sshkey_path)
		if err != nil {
			return err
		}

		sshkey_contents := string(sshkey_file)

		sshkey, err := digitalocean.NewSshKey(ctx, commonName, &digitalocean.SshKeyArgs{
			Name:      pulumi.String(commonName),
			PublicKey: pulumi.String(sshkey_contents),
		})

		myIp, err := getMyIp()
		if err != nil {
			return err
		}

		myIpCidr := fmt.Sprintf("%v/32", myIp)

		promtag, err := digitalocean.NewTag(ctx, "prometheus", &digitalocean.TagArgs{
			Name: pulumi.String("prometheus"),
		})

		if err != nil {
			return err
		}

		nodeex_tag, err := digitalocean.NewTag(ctx, "node_exporter", &digitalocean.TagArgs{
			Name: pulumi.String("node_exporter"),
		})

		if err != nil {
			return err
		}

		droplet, err := digitalocean.NewDroplet(ctx, commonName, &digitalocean.DropletArgs{
			Name:     pulumi.String(commonName),
			Image:    pulumi.String("ubuntu-20-04-x64"),
			Region:   pulumi.String("fra1"),
			Size:     pulumi.String("s-1vcpu-1gb"),
			UserData: pulumi.String(cloudconfig.Rendered),
			SshKeys: pulumi.StringArray{
				pulumi.StringOutput(sshkey.Fingerprint),
			},
			Tags: pulumi.StringArray{
				promtag.ID(),
				nodeex_tag.ID(),
			},
		})

		droplet_id := pulumi.Sprintf("%v", droplet.ID())

		_, err = digitalocean.NewFirewall(ctx, commonName, &digitalocean.FirewallArgs{
			Name: pulumi.String(commonName),
			DropletIds: pulumi.IntArray{
				droplet_id.ApplyInt(func(id string) int {
					var idInt int
					idInt, err := strconv.Atoi(id)
					if err != nil {
						fmt.Println(err)
						return idInt
					}
					return idInt
				}),
			},
			InboundRules: digitalocean.FirewallInboundRuleArray{
				digitalocean.FirewallInboundRuleArgs{
					Protocol:  pulumi.String("tcp"),
					PortRange: pulumi.String("22"),
					SourceAddresses: pulumi.StringArray{
						pulumi.String(myIpCidr),
					},
				},
				digitalocean.FirewallInboundRuleArgs{
					Protocol:  pulumi.String("tcp"),
					PortRange: pulumi.String("9100"),
					SourceAddresses: pulumi.StringArray{
						pulumi.String(myIpCidr),
					},
				},
			},
			OutboundRules: digitalocean.FirewallOutboundRuleArray{
				digitalocean.FirewallOutboundRuleArgs{
					Protocol:  pulumi.String("tcp"),
					PortRange: pulumi.String("1-65535"),
					DestinationAddresses: pulumi.StringArray{
						pulumi.String("0.0.0.0/0"),
						pulumi.String("::/0"),
					},
				},
				digitalocean.FirewallOutboundRuleArgs{
					Protocol:  pulumi.String("udp"),
					PortRange: pulumi.String("1-65535"),
					DestinationAddresses: pulumi.StringArray{
						pulumi.String("0.0.0.0/0"),
						pulumi.String("::/0"),
					},
				},
				digitalocean.FirewallOutboundRuleArgs{
					Protocol: pulumi.String("icmp"),
					DestinationAddresses: pulumi.StringArray{
						pulumi.String("0.0.0.0/0"),
						pulumi.String("::/0"),
					},
				},
			},
		})

		ctx.Export("publicIp", droplet.Ipv4Address)

		return nil
	})
}
