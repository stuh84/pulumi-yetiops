import * as pulumi from "@pulumi/pulumi";
import * as aws from "@pulumi/aws";
import * as awsx from "@pulumi/awsx";
import * as cloudinit from "@pulumi/cloudinit";
import * as fs from "fs";

let config = new pulumi.Config();

let common_name = config.require("common_name");

const size = "t4g.micro";

const ami = pulumi.output(aws.ec2.getAmi({
    filters: [{
        name: "name",
        values: [
            "ubuntu/images/hvm-ssd/ubuntu-focal-20.04-arm64-server-*"
        ]
    }],
    owners: [
        "099720109477"
    ],
    mostRecent: true,
}));

const group = new aws.ec2.SecurityGroup(common_name, {
    ingress: [
        {
            protocol: "tcp",
            fromPort: 22,
            toPort: 22,
            cidrBlocks: [
                "86.29.247.18/32"
            ]
        },
        {
            protocol: "tcp",
            fromPort: 9100,
            toPort: 9100,
            cidrBlocks: [
                "86.29.247.18/32"
            ]
        },
    ],
    egress: [
        {
            protocol: "-1",
            fromPort: 0,
            toPort: 0,
            cidrBlocks: [
                "0.0.0.0/0"
            ]
        }
    ]
});

const homedir = require('os').homedir();

var sshkey_path = `${homedir}/.ssh/id_rsa.pub`;

const sshkey_file = fs.readFileSync(sshkey_path, 'utf8');

const sshkey = new aws.ec2.KeyPair(common_name, {
    keyName: common_name,
    publicKey: sshkey_file
});

const cloudconfig = pulumi.output(cloudinit.getConfig({
    base64Encode: false,
    gzip: false,
    parts: [{
        content: "#cloudconfig\npackages:\n- prometheus-node-exporter",
        contentType: "text/cloud-config",
        filename: "init.cfg"
    }],
}, { async: true }));

const instance = new aws.ec2.Instance(common_name, {
    instanceType: size,
    vpcSecurityGroupIds: [
        group.id
    ],
    ami: ami.id,
    tags: {
        Name: common_name,
        node_exporter: "true",
        prometheus: "true"
    },
    keyName: sshkey.keyName,
    userData: cloudconfig.rendered
});

export const publicIp = instance.publicIp;
export const publicHostName = instance.publicDns;
