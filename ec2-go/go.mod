module basic-ec2

go 1.14

require (
	github.com/pulumi/pulumi-aws/sdk/v3 v3.26.1
	github.com/pulumi/pulumi-cloudinit/sdk v0.2.1 // indirect
	github.com/pulumi/pulumi/sdk/v2 v2.20.0
)
