package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"os/user"
	"strconv"
	"strings"

	"github.com/pulumi/pulumi-aws/sdk/v3/go/aws"
	"github.com/pulumi/pulumi-aws/sdk/v3/go/aws/ec2"
	"github.com/pulumi/pulumi-cloudinit/sdk/go/cloudinit"
	"github.com/pulumi/pulumi/sdk/v2/go/pulumi"
	"github.com/pulumi/pulumi/sdk/v2/go/pulumi/config"
)

func getMyIp() (string, error) {
	resp, err := http.Get("https://ifconfig.co")
	if err != nil {
		return "", err
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}

	MyIp := strings.TrimSuffix(string(body), "\n")

	return MyIp, nil
}

func main() {
	pulumi.Run(func(ctx *pulumi.Context) error {
		conf := config.New(ctx, "")
		commonName := conf.Require("common_name")

		b64encEnable := false
		gzipEnable := false
		contentType := "text/cloud-config"
		fileName := "init.cfg"
		cloudconfig, err := cloudinit.LookupConfig(ctx, &cloudinit.LookupConfigArgs{
			Base64Encode: &b64encEnable,
			Gzip:         &gzipEnable,
			Parts: []cloudinit.GetConfigPart{
				cloudinit.GetConfigPart{
					Content: "#cloud-config\n" +
						"packages:\n" +
						"- prometheus-node-exporter",
					ContentType: &contentType,
					Filename:    &fileName,
				},
			},
		}, nil)

		if err != nil {
			return err
		}

		user, err := user.Current()

		if err != nil {
			return err
		}

		sshkey_path := fmt.Sprintf("%v/.ssh/id_rsa.pub", user.HomeDir)

		sshkey_file, err := ioutil.ReadFile(sshkey_path)
		if err != nil {
			return err
		}

		sshkey_contents := string(sshkey_file)

		sshkey, err := ec2.NewKeyPair(ctx, commonName, &ec2.KeyPairArgs{
			PublicKey: pulumi.String(sshkey_contents),
		})

		if err != nil {
			return err
		}

		myIp, err := getMyIp()
		if err != nil {
			return err
		}

		myIpCidr := fmt.Sprintf("%v/32", myIp)

		group, err := ec2.NewSecurityGroup(ctx, commonName, &ec2.SecurityGroupArgs{
			Ingress: ec2.SecurityGroupIngressArray{
				ec2.SecurityGroupIngressArgs{
					Protocol: pulumi.String("tcp"),
					FromPort: pulumi.Int(22),
					ToPort:   pulumi.Int(22),
					CidrBlocks: pulumi.StringArray{
						pulumi.String(myIpCidr)},
				},
				ec2.SecurityGroupIngressArgs{
					Protocol: pulumi.String("tcp"),
					FromPort: pulumi.Int(9100),
					ToPort:   pulumi.Int(9100),
					CidrBlocks: pulumi.StringArray{
						pulumi.String(myIpCidr)},
				},
			},
			Egress: ec2.SecurityGroupEgressArray{
				ec2.SecurityGroupEgressArgs{
					Protocol: pulumi.String("-1"),
					FromPort: pulumi.Int(0),
					ToPort:   pulumi.Int(0),
					CidrBlocks: pulumi.StringArray{
						pulumi.String("0.0.0.0/0")},
				},
			},
		})

		if err != nil {
			return err
		}

		mostRecent := true
		ami, err := aws.GetAmi(ctx, &aws.GetAmiArgs{
			Filters: []aws.GetAmiFilter{
				{
					Name:   "name",
					Values: []string{"ubuntu/images/hvm-ssd/ubuntu-focal-20.04-arm64-server-*"},
				},
			},
			Owners:     []string{"099720109477"},
			MostRecent: &mostRecent,
		})

		if err != nil {
			return err
		}

		srvMap := make([]*ec2.Instance, 3)

		for i := 0; i < 3; i++ {
			index := strconv.Itoa(i)
			srv, err := ec2.NewInstance(ctx, commonName+index, &ec2.InstanceArgs{
				Tags:                pulumi.StringMap{"Name": pulumi.String(commonName + index)},
				InstanceType:        pulumi.String("t4g.micro"),
				VpcSecurityGroupIds: pulumi.StringArray{group.ID()},
				Ami:                 pulumi.String(ami.Id),
				KeyName:             pulumi.StringOutput(sshkey.KeyName),
				UserData:            pulumi.String(cloudconfig.Rendered),
			})

			if err != nil {
				return err
			}

			srvMap[i] = srv
		}

		var srvPublicIPs pulumi.StringArray

		for _, srv := range srvMap {
			srvPublicIPs = append(srvPublicIPs, srv.PublicIp)
		}

		ctx.Export("publicIps", srvPublicIPs)

		return nil

	})
}
