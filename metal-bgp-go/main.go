package main

import (
	"fmt"
	"io/ioutil"
	"os/user"

	metal "github.com/pulumi/pulumi-equinix-metal/sdk/v2/go/equinix"
	"github.com/pulumi/pulumi/sdk/v3/go/pulumi"
	"github.com/pulumi/pulumi/sdk/v3/go/pulumi/config"
)

func main() {
	pulumi.Run(func(ctx *pulumi.Context) error {
		conf := config.New(ctx, "")
		commonName := conf.Require("common_name")

		project, err := metal.NewProject(ctx, commonName, &metal.ProjectArgs{
			Name: pulumi.String(commonName),
			BgpConfig: metal.ProjectBgpConfigArgs{
				Asn:            pulumi.Int(65000),
				DeploymentType: pulumi.String("local"),
			},
		})
		if err != nil {
			return err
		}

		projectId := pulumi.Sprintf("%v", project.ID())

		user, err := user.Current()

		if err != nil {
			return err
		}

		sshkey_path := fmt.Sprintf("%v/.ssh/id_rsa.pub", user.HomeDir)

		sshkey_file, err := ioutil.ReadFile(sshkey_path)
		if err != nil {
			return err
		}

		sshkey_contents := string(sshkey_file)

		sshkey, err := metal.NewProjectSshKey(ctx, commonName, &metal.ProjectSshKeyArgs{
			Name:      pulumi.String(commonName),
			PublicKey: pulumi.String(sshkey_contents),
			ProjectId: projectId,
		})

		saltCloudInit := saltMasterCloudInitConfig()

		saltMaster, err := metal.NewDevice(ctx, "salt-master", &metal.DeviceArgs{
			Hostname:        pulumi.String("salt-master"),
			Plan:            pulumi.String("c3.small.x86"),
			Metro:           pulumi.String("am"),
			OperatingSystem: pulumi.String("ubuntu_20_04"),
			BillingCycle:    pulumi.String("hourly"),
			ProjectId:       projectId,
			ProjectSshKeyIds: pulumi.StringArray{
				sshkey.ID(),
			},
			UserData: pulumi.String(saltCloudInit),
		})

		if err != nil {
			return err
		}

		anycastIpAllocation, err := metal.NewReservedIpBlock(ctx, "anycast-ip", &metal.ReservedIpBlockArgs{
			ProjectId: projectId,
			Metro:     pulumi.String("am"),
			Type:      pulumi.String("public_ipv4"),
			Quantity:  pulumi.Int(1),
		})

		if err != nil {
			return err
		}

		bgpCloudConfig := pulumi.All(
			anycastIpAllocation.Address,
			anycastIpAllocation.Netmask,
			saltMaster.AccessPrivateIpv4).ApplyT(
			func(args []interface{}) string {
				bgpCloudInitNodeConfig := BgpNodeConfig{
					anycastIP:     args[0].(string),
					anycastIPMask: args[1].(string),
					saltMasterIP:  args[2].(string),
				}
				return bgpCloudInitConfig(&bgpCloudInitNodeConfig)
			})

		if err != nil {
			return err
		}

		bgpPrimary, err := metal.NewDevice(ctx, "bgp-01", &metal.DeviceArgs{
			Hostname:        pulumi.String("bgp-01"),
			Plan:            pulumi.String("c3.small.x86"),
			Metro:           pulumi.String("am"),
			OperatingSystem: pulumi.String("debian_10"),
			BillingCycle:    pulumi.String("hourly"),
			ProjectId:       projectId,
			ProjectSshKeyIds: pulumi.StringArray{
				sshkey.ID(),
			},
			UserData: pulumi.StringOutput(pulumi.Sprintf("%v", bgpCloudConfig)),
		})

		if err != nil {
			return err
		}

		bgpPrimarySession, err := metal.NewBgpSession(ctx, "bgp-01_BGP_SESSION", &metal.BgpSessionArgs{
			AddressFamily: pulumi.String("ipv4"),
			DeviceId:      bgpPrimary.ID(),
		})

		if err != nil {
			return err
		}

		bgpSecondary, err := metal.NewDevice(ctx, "bgp-02", &metal.DeviceArgs{
			Hostname:        pulumi.String("bgp-02"),
			Plan:            pulumi.String("c3.small.x86"),
			Metro:           pulumi.String("am"),
			OperatingSystem: pulumi.String("debian_10"),
			BillingCycle:    pulumi.String("hourly"),
			ProjectId:       projectId,
			ProjectSshKeyIds: pulumi.StringArray{
				sshkey.ID(),
			},
			UserData: pulumi.StringOutput(pulumi.Sprintf("%v", bgpCloudConfig)),
		})

		if err != nil {
			return err
		}

		bgpSecondarySession, err := metal.NewBgpSession(ctx, "bgp-02_BGP_SESSION", &metal.BgpSessionArgs{
			AddressFamily: pulumi.String("ipv4"),
			DeviceId:      bgpSecondary.ID(),
		})

		if err != nil {
			return err
		}

		ctx.Export("projectName", project.Name)
		ctx.Export("saltMasterIP", saltMaster.AccessPublicIpv4)
		ctx.Export("bgpPrimaryIP", bgpPrimary.AccessPublicIpv4)
		ctx.Export("bgpSecondaryIP", bgpSecondary.AccessPublicIpv4)
		ctx.Export("bgpPrimarySessionStatus", bgpPrimarySession.Status)
		ctx.Export("bgpSecondarySessionStatus", bgpSecondarySession.Status)
		ctx.Export("anycastIP", anycastIpAllocation.Address)
		return nil
	})
}
