package main

import (
	"fmt"

	"github.com/juju/juju/cloudconfig/cloudinit"
)

type BgpNodeConfig struct {
	anycastIP     string
	anycastIPMask string
	saltMasterIP  string
}

func saltMasterCloudInitConfig() string {
	c, err := cloudinit.New("focal")

	if err != nil {
		panic(err)
	}

	c.AddRunCmd("curl -fsSL https://bootstrap.saltproject.io -o install_salt.sh")
	c.AddRunCmd("sh install_salt.sh -P -M -x python3")

	c.AddRunTextFile("/etc/salt/master.d/master.conf", `autosign_grains_dir: /etc/salt/autosign-grains
pillar_roots:
  base:
    - /srv/salt/pillars

file_roots:
  base:
    - /srv/salt/states`, 0644)

	c.AddRunTextFile("/etc/salt/minion.d/minion.conf", `autosign_grains:
- role
startup_states: highstate
grains:
  role: master`, 0644)

	c.AddRunCmd("mkdir -p /etc/salt/autosign-grains")

	c.AddRunCmd("echo -e \"master\nbgp\n\" > /etc/salt/autosign-grains/role")

	c.AddRunCmd("PRIVATE_IP=$(curl -s https://metadata.platformequinix.com/metadata | jq -r '.network.addresses | map(select(.public==false)) | first | .address')")

	c.AddRunCmd("mkdir -p /srv/salt")
	c.AddRunCmd("git clone https://gitlab.com/stuh84/salt-anycast-equinix /srv/salt")

	c.AddRunCmd("echo interface: ${PRIVATE_IP} > /etc/salt/master.d/private-interface.conf")
	c.AddRunCmd("echo master: ${PRIVATE_IP} > /etc/salt/minion.d/master.conf")
	c.AddRunCmd("systemctl daemon-reload")
	c.AddRunCmd("systemctl enable salt-master.service")
	c.AddRunCmd("systemctl restart --no-block salt-master.service")
	c.AddRunCmd("systemctl enable salt-minion.service")
	c.AddRunCmd("systemctl restart --no-block salt-minion.service")

	script, err := c.RenderScript()

	if err != nil {
		panic(err)
	}

	return script
}

func bgpCloudInitConfig(config *BgpNodeConfig) string {
	c, err := cloudinit.New("focal")

	if err != nil {
		panic(err)
	}

	c.AddRunCmd("curl -fsSL https://bootstrap.saltproject.io -o install_salt.sh")
	c.AddRunCmd("sh install_salt.sh -P -x python3")

	c.AddRunCmd("PRIVATE_IP=$(curl -s https://metadata.platformequinix.com/metadata | jq -r '.network.addresses | map(select(.public==false)) | first | .address')")

	c.AddRunCmd(fmt.Sprintf("echo master: %s > /etc/salt/minion.d/master.conf", config.saltMasterIP))

	c.AddRunTextFile("/etc/salt/grains",
		fmt.Sprintf(
			"anycast:\n  ipv4:\n    - address: %s\n      mask: %s\n",
			config.anycastIP,
			config.anycastIPMask,
		), 0400,
	)

	c.AddRunTextFile("/etc/salt/minion.d/minion.conf", `autosign_grains:
- role
startup_states: highstate`, 0644)

	c.AddRunCmd("echo \"bgp:\n  localip: ${PRIVATE_IP}\nrole: bgp\" >> /etc/salt/grains")

	c.AddRunCmd("systemctl daemon-reload")
	c.AddRunCmd("systemctl enable salt-minion.service")
	c.AddRunCmd("systemctl restart --no-block salt-minion.service")

	script, err := c.RenderScript()

	if err != nil {
		panic(err)
	}

	return script
}
