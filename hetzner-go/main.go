package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"os/user"
	"strconv"
	"strings"

	"github.com/pulumi/pulumi-cloudinit/sdk/go/cloudinit"
	hcloud "github.com/pulumi/pulumi-hcloud/sdk/go/hcloud"
	"github.com/pulumi/pulumi/sdk/v2/go/pulumi"
	"github.com/pulumi/pulumi/sdk/v2/go/pulumi/config"
)

func getMyIp() (string, error) {
	resp, err := http.Get("https://ifconfig.co")
	if err != nil {
		return "", err
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}

	MyIp := strings.TrimSuffix(string(body), "\n")

	return MyIp, nil
}

func main() {
	pulumi.Run(func(ctx *pulumi.Context) error {

		conf := config.New(ctx, "")
		commonName := conf.Require("common_name")
		cloudInitPath := conf.Require("cloud_init_path")

		cloudInitScript, err := ioutil.ReadFile(cloudInitPath)

		if err != nil {
			return err
		}

		cloudInitContents := string(cloudInitScript)

		b64encEnable := false
		gzipEnable := false
		contentType := "text/cloud-config"
		fileName := "init.cfg"
		cloudconfig, err := cloudinit.LookupConfig(ctx, &cloudinit.LookupConfigArgs{
			Base64Encode: &b64encEnable,
			Gzip:         &gzipEnable,
			Parts: []cloudinit.GetConfigPart{
				cloudinit.GetConfigPart{
					Content:     cloudInitContents,
					ContentType: &contentType,
					Filename:    &fileName,
				},
			},
		}, nil)

		if err != nil {
			return err
		}

		user, err := user.Current()

		if err != nil {
			return err
		}

		sshkey_path := fmt.Sprintf("%v/.ssh/id_rsa.pub", user.HomeDir)

		sshkey_file, err := ioutil.ReadFile(sshkey_path)
		if err != nil {
			return err
		}

		sshkey_contents := string(sshkey_file)

		sshkey, err := hcloud.NewSshKey(ctx, commonName, &hcloud.SshKeyArgs{
			Name:      pulumi.String(commonName),
			PublicKey: pulumi.String(sshkey_contents),
		})

		myIp, err := getMyIp()
		if err != nil {
			return err
		}

		myIpCidr := fmt.Sprintf("%v/32", myIp)

		firewall, err := hcloud.NewFirewall(ctx, commonName, &hcloud.FirewallArgs{
			Name: pulumi.String(commonName),
			Rules: hcloud.FirewallRuleArray{
				hcloud.FirewallRuleArgs{
					Direction: pulumi.String("in"),
					Port:      pulumi.String("22"),
					Protocol:  pulumi.String("tcp"),
					SourceIps: pulumi.StringArray{
						pulumi.String(myIpCidr),
					},
				},
				hcloud.FirewallRuleArgs{
					Direction: pulumi.String("in"),
					Port:      pulumi.String("9100"),
					Protocol:  pulumi.String("tcp"),
					SourceIps: pulumi.StringArray{
						pulumi.String(myIpCidr),
					},
				},
			},
		})

		firewall_id := pulumi.Sprintf("%v", firewall.ID())

		srv, err := hcloud.NewServer(ctx, commonName, &hcloud.ServerArgs{
			Name:       pulumi.String(commonName),
			Image:      pulumi.String("ubuntu-20.04"),
			ServerType: pulumi.String("cx11"),
			Labels: pulumi.Map{
				"prometheus":    pulumi.String("true"),
				"node_exporter": pulumi.String("true"),
			},
			UserData: pulumi.String(cloudconfig.Rendered),
			SshKeys: pulumi.StringArray{
				sshkey.Name,
			},
			FirewallIds: pulumi.IntArray{
				firewall_id.ApplyInt(func(id string) int {
					var idInt int
					idInt, err := strconv.Atoi(id)
					if err != nil {
						fmt.Println(err)
						return idInt
					}
					return idInt
				}),
			},
		})

		if err != nil {
			return err
		}

		ctx.Export("publicIP", srv.Ipv4Address)

		return nil
	})
}
