module hetzner-staging

go 1.16

require (
	github.com/pulumi/pulumi-cloudinit/sdk v0.2.1
	github.com/pulumi/pulumi-hcloud/sdk v0.7.1
	github.com/pulumi/pulumi/sdk/v2 v2.24.1
)
