"""An AWS Python Pulumi program"""

import pulumi
import os
import requests
import pulumi_aws as aws
import pulumi_cloudinit as cloudinit
from pathlib import Path

def GetMyIP():
  request = requests.get("https://ifconfig.co/json")
  request_json = request.json()  
  myIp = request_json['ip']
  return myIp

config = pulumi.Config()
common_name = config.require("common_name")

size = "t4g.micro"

ami = aws.get_ami(most_recent="true",
                  owners = [
                    "099720109477"
                  ],
                  filters = [
                    {
                       "name":"name",
                       "values": [
                          "ubuntu/images/hvm-ssd/ubuntu-focal-20.04-arm64-server-*"
                       ]
                    }
                  ])
homedir = str(Path.home())

yetissh_path = "{}/.ssh/ssh_yetiops.pub".format(homedir)

if os.path.isfile(yetissh_path):
    sshkey_path = yetissh_path
else:
    sshkey_path = "{}/.ssh/id_rsa.pub".format(
                      homedir
                  )
sshkey_file = open(sshkey_path, 'r')


sshkey = aws.ec2.KeyPair(common_name,
  public_key = sshkey_file.read().strip("\n") 
)

myIpCidr = "{}/32".format(
  GetMyIP().strip("\n")
)
                  
group = aws.ec2.SecurityGroup(common_name,
    description="Yetiops Access",
    ingress = [
      {
        'protocol': 'tcp',
        'from_port': 22,
        'to_port': 22,
        'cidr_blocks': [
          myIpCidr
        ]
      },
      {
        'protocol': 'tcp',
        'from_port': 9100,
        'to_port': 9100,
        'cidr_blocks': [
          myIpCidr
        ]
      }
    ],
    egress = [
      {
        'protocol': '-1',
        'from_port': 0,
        'to_port': 0,
        'cidr_blocks': [
          "0.0.0.0/0" 
        ]
      }
    ]
  )

cloudconfig = cloudinit.get_config(
    base64_encode = False,
    gzip = False,
    parts = [
      cloudinit.GetConfigPartArgs(
        content = "#cloudconfig\npackages:\n- prometheus-node-exporter",
        content_type = "text/cloud-config",
        filename = "init.cfg"
      )
    ]
  )


instance = aws.ec2.Instance(common_name,
  instance_type = size,
  vpc_security_group_ids = [
    group.id
  ],
  ami = ami.id,
  tags = {
    "Name": common_name,
    "prometheus": "true",
    "node_exporter": "true"
  },
  key_name = sshkey.key_name,
  user_data = cloudconfig.rendered
)

pulumi.export('publicIp', instance.public_ip)
pulumi.export('publicHostName', instance.public_dns)
